package net.therap.controller;

import net.therap.domain.Request;
import net.therap.domain.Summary;
import net.therap.service.InputHandler;
import org.apache.commons.cli.*;

import java.util.*;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */

class CliHandler {
    public boolean isToSort(String[] args) {
        Options options = new Options();
        Option toSortFlag = Option.builder()
                .longOpt("sort")
                .hasArg(false)
                .desc("sort the data")
                .build();

        options.addOption(toSortFlag);

        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("sort")) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}

public class LogDataParser implements Comparator<Summary> {
    public static final int TOTAL_HOURS = 24;
    public static final int MID_DAY_HOUR = 12;

    private Map<Integer, Integer> getRequestCounts;
    private Map<Integer, Integer> postRequestCounts;
    private Map<Integer, TreeSet<String>> uris;
    private Map<Integer, Integer> totalResponseTimes;

    private List<Request> requests;
    private InputHandler inputHandler;
    private CliHandler cliHandler;

    public LogDataParser() {
        this.getRequestCounts = new HashMap<>();
        this.postRequestCounts = new HashMap<>();
        this.uris = new HashMap<>();
        this.totalResponseTimes = new HashMap<>();

        requests = new ArrayList<>();
        inputHandler = new InputHandler();
        cliHandler = new CliHandler();
    }

    public void inputRequests() {
        while (inputHandler.hasRequestFromLog()) {
            Request request = inputHandler.getRequestFromLog();
            if (request != null) {
                requests.add(request);
            }
        }
    }

    public void processRequests() {
        for (Request request : requests) {
            int hour = request.getHour();

            if (request.isGetRequest()) {
                int previousCount = getRequestCounts.getOrDefault(hour, 0);
                getRequestCounts.put(hour, previousCount + 1);
            } else {
                int previousCount = postRequestCounts.getOrDefault(hour, 0);
                postRequestCounts.put(hour, previousCount + 1);
            }

            if (!uris.containsKey(hour)) {
                uris.put(hour, new TreeSet<>());
            }
            uris.get(hour).add(request.getUri());

            int previousTotalResponseTime = totalResponseTimes.getOrDefault(hour, 0);
            totalResponseTimes.put(hour, previousTotalResponseTime + request.getResponseTime());

        }
    }

    public List<Request> getRequests() {
        return requests;
    }

    @Override
    public int compare(Summary o1, Summary o2) {
        return (o1.getGetCount() + o1.getPostCount()) - (o2.getGetCount() + o2.getPostCount());
    }

    private Integer getTotalResponseTime(Integer hour) {
        return totalResponseTimes.getOrDefault(hour, 0);
    }

    private Integer getUniqueURICount(Integer hour) {
        return uris.getOrDefault(hour, new TreeSet<>()).size();
    }

    private Integer getPostRequestCount(Integer hour) {
        return postRequestCounts.getOrDefault(hour, 0);
    }

    private Integer getGetRequestCount(Integer hour) {
        return getRequestCounts.getOrDefault(hour, 0);
    }

    private Integer convertTo12Hrs(Integer hour) {
        if (hour > LogDataParser.MID_DAY_HOUR) {
            hour -= LogDataParser.MID_DAY_HOUR;
        }

        if (hour == 0) {
            hour = LogDataParser.MID_DAY_HOUR;
        }

        return hour;
    }

    private String getHourText(Integer startHour) {
        String startMarker = "am";
        if (startHour >= LogDataParser.MID_DAY_HOUR) {
            startMarker = "pm";
        }

        Integer finishHour = startHour + 1;
        if (finishHour == LogDataParser.TOTAL_HOURS) {
            finishHour = 0;
        }
        String finishMarker = "am";
        if (finishHour >= LogDataParser.MID_DAY_HOUR) {
            finishMarker = "pm";
        }

        return String.format("%d.00 %s - %d.00 %s", convertTo12Hrs(startHour), startMarker, convertTo12Hrs(finishHour), finishMarker);
    }

    public List<Summary> generateSummary(boolean isToSort) {
        List<Summary> summaries = new ArrayList<>();

        ArrayList<Integer> hours = new ArrayList<>();
        for (int hour = 0; hour < TOTAL_HOURS; hour++) {
            hours.add(hour);
        }

        for (Integer hour : hours) {
            Summary summary = new Summary(getHourText(hour), getGetRequestCount(hour), getPostRequestCount(hour),
                    getUniqueURICount(hour), getTotalResponseTime(hour));
            summaries.add(summary);
        }

        if (isToSort) {
            Collections.sort(summaries, this);
        }
        return summaries;
    }

    public boolean canHandleLogInput(String[] args) {
        return inputHandler.canHandleLogInput(args);
    }

    public boolean isToSort(String[] args) {
        return cliHandler.isToSort(args);
    }
}

