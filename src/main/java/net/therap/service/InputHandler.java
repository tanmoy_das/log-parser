package net.therap.service;

import net.therap.controller.LogDataParser;
import net.therap.domain.Request;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class InputHandler {
    private Scanner logScanner;
    private String logPath;
    private String[] args;

    public InputHandler() {
    }

    public boolean canHandleLogInput(String[] args) {
        if (args.length == 0) {
            System.out.println("Log File Path not given");
            return false;
        }
        logPath = args[0];

        try {
            logScanner = new Scanner(new File(logPath));
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("Log file not found at " + logPath);
            return false;
        }
    }

    public Request getRequestFromLog() {
        String requestData = logScanner.nextLine();

        if (requestData.contains("INFO")) {
            return null;
        }
        if (!requestData.contains("[PROFILER:132]")) {
            return null;
        }

        return parseRequest(requestData);
    }

    private Request parseRequest(String requestData) {
        try {
            int i = 0;
            for (; i < requestData.length(); i++) {
                if (requestData.charAt(i) == ' ') break;
            }

            int hour = 0;
            for (i++; requestData.charAt(i) != ':'; i++) {
                hour *= 10;
                hour += Character.getNumericValue(requestData.charAt(i)) - Character.getNumericValue('0');
            }

            while (!requestData.substring(i).startsWith("URI=[")) {
                i++;
            }
            int endPosition = requestData.indexOf(']', i + 5);
            String uri = requestData.substring(i + 5, endPosition);

            i = endPosition + 3;
            boolean getRequest = true;
            if (requestData.charAt(i) == 'P') {
                getRequest = false;
            }

            int responseTime = 0;
            for (; i < requestData.length(); i++) {
                Character ch = requestData.charAt(i);
                if (Character.isDigit(ch)) {
                    responseTime *= 10;
                    responseTime += Character.getNumericValue(ch) - Character.getNumericValue('0');
                }
            }

            return new Request(hour, getRequest, uri, responseTime);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean hasRequestFromLog() {
        return logScanner.hasNextLine();
    }
}
