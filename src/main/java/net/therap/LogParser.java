package net.therap;

import net.therap.controller.LogDataParser;
import net.therap.domain.Summary;
import net.therap.view.SummaryPrinter;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class LogParser {
    public static void main(String[] args) {
        LogDataParser logDataParser = new LogDataParser();

        if (logDataParser.canHandleLogInput(args)) {
            logDataParser.inputRequests();
            logDataParser.processRequests();

            boolean isToSort = logDataParser.isToSort(args);
            List<Summary> summaries = logDataParser.generateSummary(isToSort);

            SummaryPrinter summaryPrinter = new SummaryPrinter();
            summaryPrinter.display(summaries);

        } else {
            System.out.println("Log Parsing Failed");
        }
    }
}

