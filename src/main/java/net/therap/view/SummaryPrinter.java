package net.therap.view;

import net.therap.domain.Summary;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class SummaryPrinter {

    public void display(List<Summary> summaries) {
        String header;
        header = String.format("| %19s | GET/POST Count | Unique URI Count | Total Response Time |%n", "Time");
        String hyphenLine = "";
        for (int i = 0; i < header.length(); i++) {
            hyphenLine = hyphenLine + "-";
        }

        System.out.println(hyphenLine);
        System.out.println(header);
        System.out.println(hyphenLine);

        for (Summary summary : summaries) {
            String requestCountTxt = String.format("%d/%d", summary.getGetCount(), summary.getPostCount());
            System.out.printf("| %19s | %14s | %16d | %19d |%n", summary.getTimeTxt(), requestCountTxt,
                    summary.getUriCount(), summary.getTotalResponseTime());
            System.out.println(hyphenLine);
        }
    }
}
