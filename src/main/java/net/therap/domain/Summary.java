package net.therap.domain;

public class Summary {
    private String timeTxt;
    private Integer getCount;
    private Integer postCount;
    private Integer uriCount;
    private Integer totalResponseTime;

    public Summary(String timeTxt, Integer getCount, Integer postCount, Integer uriCount, Integer totalResponseTime) {
        this.timeTxt = timeTxt;
        this.getCount = getCount;
        this.postCount = postCount;
        this.uriCount = uriCount;
        this.totalResponseTime = totalResponseTime;
    }

    public String getTimeTxt() {
        return timeTxt;
    }

    public void setTimeTxt(String timeTxt) {
        this.timeTxt = timeTxt;
    }

    public Integer getGetCount() {
        return getCount;
    }

    public void setGetCount(Integer getCount) {
        this.getCount = getCount;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public Integer getUriCount() {
        return uriCount;
    }

    public void setUriCount(Integer uriCount) {
        this.uriCount = uriCount;
    }

    public Integer getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(Integer totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }
}
