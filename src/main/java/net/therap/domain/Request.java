package net.therap.domain;

public class Request {
    private int hour;
    private boolean getRequest;
    private String uri;
    private int responseTime;

    public Request(int hour, boolean getRequest, String uri, int responseTime) {
        this.hour = hour;
        this.getRequest = getRequest;
        this.uri = uri;
        this.responseTime = responseTime;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public boolean isGetRequest() {
        return getRequest;
    }

    public void setGetRequest(boolean getRequest) {
        this.getRequest = getRequest;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(int responseTime) {
        this.responseTime = responseTime;
    }
}
